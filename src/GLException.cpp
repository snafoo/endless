#include "GLException.h"

GLException::GLException(const std::string& function, GLenum error)
: m_error(error) {
	std::string errorString;

	switch (m_error) {
		case GL_INVALID_ENUM:
			errorString.assign("An unacceptable value is specified for an enumerated argument.");
            break;
		case GL_INVALID_VALUE:
			errorString.assign("A numeric argument is out of range.");
            break;
		case GL_INVALID_OPERATION:
			errorString.assign("The specified operation is not allowed in the current state.");
            break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			errorString.assign("The framebuffer object is not complete.");
            break;
		case GL_OUT_OF_MEMORY:
			errorString.assign("There is not enough memory left to execute the command.");
            break;
	}

	SetMessage(function + ": " + errorString);
}

GLException::~GLException() {

}
