#ifndef SPRITE_H
#define SPRITE_H

#include <iostream>
#include <array>
#include <memory>

#include "Shader.h"
#include "ShaderProgram.h"
#include "GLHeaders.h"

class Sprite {
public:
	Sprite();
	~Sprite();

	void Init(float x, float y, float width, float height);
	void Draw();
protected:
	struct Position {
		int x;
		int y;
		int width;
		int height;
	};

	GLuint m_vboID = 0;
	GLuint m_vaoID = 0;

	Position m_position;

	std::shared_ptr<Shader> m_vertexShader;
	std::shared_ptr<Shader> m_fragmentShader;
};
#endif
