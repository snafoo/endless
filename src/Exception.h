#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <iostream>
#include <string>
#include <exception>

class Exception : public std::exception {
public:
	Exception();
	explicit Exception(const std::string& message);
	virtual ~Exception(); 

	Exception(const Exception& other);

	virtual const char* what() const noexcept override { return m_message.c_str(); }
protected:
	void SetMessage(const std::string& message) { m_message = message; };
private:
	std::string m_message;
};

#endif