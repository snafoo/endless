#ifndef SHADEREXCEPTION_H_
#define SHADEREXCEPTION_H_

#include <string>
#include "Exception.h"

class ShaderException : public Exception {
public:
	ShaderException(std::string log);
	virtual ~ShaderException();
protected:
	std::string m_log;
};
#endif