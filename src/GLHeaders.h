#ifdef __APPLE__
#include <OpenGL/gl3.h>
#include <OpenGL/glext.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

#include <iostream>
#include "GLException.h"

#ifdef GL_DEBUG
#define CHECK_GL_ERROR(fn) {std::cout << "Checking error" << std::endl; GLenum err = glGetError(); \
			if(err != GL_NO_ERROR) {throw GlException(#fn, err);}}
#else
#define CHECK_GL_ERROR(fn)
#endif	

