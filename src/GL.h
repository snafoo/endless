#ifndef GL_H_
#define GL_H_

#include <string>
#include "GLHeaders.h"

namespace gl {
	namespace helpers {
		int GetMinorVersion();
		int GetMajorVersion();
		std::string GetVersionString();
		std::string GetGLSLVersionString();
	}
}
#endif