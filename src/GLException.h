#ifndef GLEXCEPTION_H_
#define GLEXCEPTION_H_

#include "Exception.h"
#include "GLHeaders.h"

class GLException : public Exception {
public:
	GLException(const std::string& function, GLenum error);
	virtual ~GLException();
protected:
	GLenum m_error;
};

#endif
