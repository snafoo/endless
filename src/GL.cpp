#include "GL.h"

int gl::helpers::GetMinorVersion() {
	int glVersionMinor;
	glGetIntegerv(GL_MINOR_VERSION, &glVersionMinor);
	return glVersionMinor;
}

int gl::helpers::GetMajorVersion() {
	int glVersionMajor;
	glGetIntegerv(GL_MAJOR_VERSION, &glVersionMajor);
	return glVersionMajor;
}

std::string gl::helpers::GetVersionString() {
	auto ptr = reinterpret_cast<const char*>(glGetString(GL_VERSION));
	return ptr;
}

std::string gl::helpers::GetGLSLVersionString() {
	auto ptr = reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION));
	return ptr;
}