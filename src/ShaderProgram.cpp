	#include "ShaderProgram.h"

ShaderProgram::ShaderProgram() {
	m_handle = glCreateProgram();
}

ShaderProgram::~ShaderProgram() {
	Destroy();
}

void ShaderProgram::Attach(Shader* shader) {
	glAttachShader(m_handle, shader->Handle());
	CHECK_GL_ERROR(glAttachShader);
	m_shaders.push_back(shader);
}

void ShaderProgram::Link() {
	glLinkProgram(m_handle);
	CHECK_GL_ERROR(glLinkProgram);
	
	if (!IsLinked()) {
		throw ShaderException(GetInfoLog());
	}
}

bool ShaderProgram::IsLinked() const {
	GLint result;
	glGetProgramiv(m_handle, GL_LINK_STATUS, &result);
	return result;
}

std::string ShaderProgram::GetInfoLog() const {
	int length = 0;
	glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &length);
	CHECK_GL_ERROR(glGetProgramiv);

	std::string log;
	if (length > 0) {
		auto text = std::unique_ptr<char []>(new char[length]);
		int charsCopied = 0;
		glGetProgramInfoLog(m_handle, length, &charsCopied, text.get());
		CHECK_GL_ERROR(glGetProgramiv);
		log = text.get();
	}
	return log;
}

void ShaderProgram::Destroy() {
	if (m_handle != 0) {
		glDeleteProgram(m_handle);
		CHECK_GL_ERROR(glDeleteProgram);
	}

	for (Shader* shader : m_shaders) {
		delete shader;
	}
}