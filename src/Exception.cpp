#include "Exception.h"

Exception::Exception() {

}

Exception::Exception(const std::string& message)
: m_message(message) { 
}

Exception::Exception(const Exception& other)
: m_message(other.m_message) {
}

Exception::~Exception() {
}