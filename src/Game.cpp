#include "Game.h"

Game::Game()
{
}

Game::~Game() {

}

void Game::Initialize() {
    sf::ContextSettings settings;
    settings.majorVersion = 3;
    settings.minorVersion = 3;
    settings.attributeFlags = sf::ContextSettings::Core;

    m_window.create(sf::VideoMode(800, 600), "Endless Online", sf::Style::Default, settings);

#ifndef __APPLE__
    InitializeGlew();
#endif

    glClearColor(0.0f, 0.0f, 1.0f, 2.0f);

    m_sprite.Init(-1, -1, 1, 1);
    m_texture.LoadFromFile("1.jpg");
}   
    
void Game::Run() {
	Initialize();
	GameLoop();
}

void Game::Draw() {
	glClearDepth(1.0);
    
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_sprite.Draw();

    m_window.display();  
}

void Game::GameLoop() {
    bool isRunning = true;

    while (isRunning == true) {
        sf::Event event;

        while (m_window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                isRunning = false;
            }
        }

        Draw();
    }
} 
