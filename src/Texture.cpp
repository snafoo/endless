#include "Texture.h"

Texture::Texture() {
	Init();
}

Texture::~Texture() {
	Destroy();
}

void Texture::Destroy() {
	if (m_handle != 0) {
		glDeleteTextures(1, &m_handle);
		CHECK_GL_ERROR(glDeleteTextures);
	}
}

void Texture::Init() {
	if (m_handle == 0) {
		glGenTextures(1, &m_handle);
		CHECK_GL_ERROR(glGenTextures);
	}
}

void Texture::LoadFromFile(const std::string& filename) {
	sf::Image image;
	if (image.loadFromFile(filename)) {
		glBindTexture(GL_TEXTURE_2D, m_handle);
		CHECK_GL_ERROR(glBindTexture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.getSize().x, image.getSize().y, 0, GL_RGB, GL_UNSIGNED_BYTE, image.getPixelsPtr());	
	}
}