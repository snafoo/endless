#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <string>

#include <SFML/Graphics.hpp>
#include "GLHeaders.h"
#include "GLObject.h"

class Texture : public GLObject {
public:
	Texture();
	~Texture();

	void LoadFromFile(const std::string& filename);
protected:
	void Init();
	virtual void Destroy();
};

#endif