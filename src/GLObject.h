#ifndef GLOBJECT_H_
#define GLOBJECT_H_

#include "GLHeaders.h"

class GLObject {
public:
	virtual ~GLObject();

	GLuint Handle() const { return m_handle; }
protected:
	GLObject();

	GLuint m_handle = 0;

	virtual void Destroy() = 0;
};

#endif