#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <SFML/Window.hpp>

#include "GL.h"
#include "GLHeaders.h"
#include "Sprite.h"
#include "Texture.h"

class Game {
public:
	Game();
	~Game();

	void Run();
protected:
	sf::Window m_window;

	Sprite m_sprite;
	Texture m_texture;

	void Initialize();
	void GameLoop();
	void Draw();

#ifndef __APPLE__
    void InitializeGlew() {
	    glewExperimental = GL_TRUE;
	    if(glewInit() != GLEW_OK)
	    {
	        throw std::runtime_error("GLEW could not be initialized");
	    }
	    glGetError();
	}
#endif
};

#endif
