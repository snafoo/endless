#include "Sprite.h"

Sprite::Sprite() {
}

Sprite::~Sprite() {
	if (m_vboID != 0) {
		glDeleteBuffers(1, &m_vboID);
	}
}

void Sprite::Init(float x, float y, float width, float height) {
	m_position.x = x;
	m_position.y = y;
	m_position.width = width;
	m_position.height = height;

	if (m_vboID == 0) {
		glGenBuffers(1, &m_vboID);
	}
	
	std::array<GLfloat, 12> vertex;
	vertex[0] = x + width;
	vertex[1] = y + height;

	vertex[2] = x;
	vertex[3] = y + height;

	vertex[4] = x;
	vertex[5] = y;

	vertex[6] = x;
	vertex[7] = y;

	vertex[8] = x + width;
	vertex[9] = y;

	vertex[10] = x + width;
	vertex[11] = y + height;

	glGenVertexArrays(1, &m_vaoID);
	CHECK_GL_ERROR(glGenVertexArrays);

	glBindVertexArray(m_vaoID);
	CHECK_GL_ERROR(glBindVertexArray);

	glBindBuffer(GL_ARRAY_BUFFER, m_vboID);
	CHECK_GL_ERROR(glBindBuffer);

	glBufferData(GL_ARRAY_BUFFER, vertex.size() * sizeof(GLfloat), &vertex, GL_STATIC_DRAW);
	CHECK_GL_ERROR(glBufferData);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0));
	CHECK_GL_ERROR(glVertexAttribPointer);

	glEnableVertexAttribArray(0);
	glBindVertexArray(0);

    const std::string vertexShaderSrc = R"(
    #version 330

    layout (location = 0) in vec2 position;

    void main() {   
        gl_Position = vec4(position.x, position.y, 0, 1.0);
    }
    )";

    const std::string fragmentShaderSrc = R"(
    #version 330

    out vec4 color;

    void main() {
        color = vec4(1.0f, 0.3f, 0.5f, 1.0f);
    }
    )";

    Shader* vertexShader = new Shader(Shader::ShaderType::Vertex);
    vertexShader->CompileFromString(vertexShaderSrc);   

    Shader* fragmentShader = new Shader(Shader::ShaderType::Fragment);
    fragmentShader->CompileFromString(fragmentShaderSrc);;

    ShaderProgram shaderProgram;
    shaderProgram.Attach(vertexShader);
    shaderProgram.Attach(fragmentShader);
    shaderProgram.Link();
    shaderProgram.UseProgram();
}

void Sprite::Draw() {
	glBindVertexArray(m_vaoID);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}