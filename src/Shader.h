#ifndef SHADER_H_
#define SHADER_H_

#include <string>

#include "GLObject.h"
#include "GLHeaders.h"
#include "ShaderException.h"

class Shader : public GLObject {
public:
	enum class ShaderType : GLenum {
		Vertex = GL_VERTEX_SHADER,
		Fragment = GL_FRAGMENT_SHADER,
		Geometry = GL_GEOMETRY_SHADER_EXT
	};

	Shader(ShaderType type);
	~Shader();

	void CompileFromString(const std::string& source);

	bool IsCompiled() const;

	std::string GetLog() const;
protected:
	ShaderType m_type;

	virtual void Destroy();
};
	
#endif