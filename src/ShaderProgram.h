#ifndef SHADERPROGRAM_H_
#define SHADERPROGRAM_H_

#include <memory>
#include <vector>

#include "Shader.h"
#include "ShaderException.h"
#include "GLHeaders.h"

class ShaderProgram : public GLObject {
public:
	ShaderProgram();
	~ShaderProgram();

	void Attach(Shader* shader);

	void Link();
	bool IsLinked() const;

	void UseProgram() const { glUseProgram(m_handle); }

	std::string GetInfoLog() const;
protected:
	std::vector<Shader*> m_shaders;

	virtual void Destroy();
};

#endif