#include <iostream>
#include "Game.h"
#include "GLHeaders.h"

#define GL_DEBUG

int main(int argc, char* args[]) {
    try {
        Game game;
        game.Run();                  
    } catch (const Exception& e) {
        std::cout << e.what() << std::endl;
    }
 
    return 0;
}