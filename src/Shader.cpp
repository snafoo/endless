#include "Shader.h"

#include <memory>

Shader::Shader(ShaderType type)
: m_type(type) {
	m_handle = glCreateShader(static_cast<GLenum>(type));
	CHECK_GL_ERROR(glCreateShader);
}

Shader::~Shader() {
	Destroy();
}

void Shader::CompileFromString(const std::string &source) {
	const char* text = source.c_str();
	glShaderSource(m_handle, 1, &text, nullptr);
	CHECK_GL_ERROR(glShaderSource);
	glCompileShader(m_handle);
	CHECK_GL_ERROR(glCompileShader);

	if (!IsCompiled()) {
		throw ShaderException(GetLog());
	}
}

bool Shader::IsCompiled() const {
	GLint result = GL_FALSE;
	glGetShaderiv(m_handle, GL_COMPILE_STATUS, &result);
	CHECK_GL_ERROR(glGetShaderiv);
	return result == GL_TRUE;
}

std::string Shader::GetLog() const {
	int length = 0;
	glGetShaderiv(m_handle, GL_INFO_LOG_LENGTH, &length);
	CHECK_GL_ERROR(glGetShaderiv);

	std::string log;
	if (length > 0) {
		auto text = std::unique_ptr<char []>(new char[length]);
		int charsCopied = 0;
		glGetShaderInfoLog(m_handle, length, &charsCopied, text.get());
		CHECK_GL_ERROR(glGetShaderInfoLog);
		log = text.get();
	}
	return log;
}

void Shader::Destroy() {
	if (m_handle != 0) {
		glDeleteShader(m_handle);
		CHECK_GL_ERROR(glDeleteShader);
	}
}
